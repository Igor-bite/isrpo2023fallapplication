package com.klyuzhevigor.isrpo.mapper;

import com.klyuzhevigor.isrpo.dal.NoteEntity;
import com.klyuzhevigor.isrpo.model.NewNote;
import com.klyuzhevigor.isrpo.model.Note;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface NoteMapper {

    NoteMapper INSTANCE = Mappers.getMapper(NoteMapper.class);

    Note toNote(NoteEntity entity);
    NoteEntity toEntity(NewNote newNote);
    NoteEntity toEntity(Long id, NewNote newNote);
}
