package com.klyuzhevigor.isrpo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Isrpo2023FallApplication {

    public static void main(String[] args) {
        SpringApplication.run(Isrpo2023FallApplication.class, args);
    }

}
