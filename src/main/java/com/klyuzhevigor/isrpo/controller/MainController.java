package com.klyuzhevigor.isrpo.controller;

import com.klyuzhevigor.isrpo.api.NoteApi;
import com.klyuzhevigor.isrpo.dal.NoteRepository;
import com.klyuzhevigor.isrpo.mapper.NoteMapper;
import com.klyuzhevigor.isrpo.model.NewNote;
import com.klyuzhevigor.isrpo.model.Note;
import im.aop.loggers.Level;
import im.aop.loggers.advice.around.LogAround;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class MainController implements NoteApi {

    private final NoteRepository noteRepository;

    @LogAround(level = Level.INFO)
    @Override
    public ResponseEntity<Note> create(NewNote newNote) {
        return ResponseEntity.ok(
                NoteMapper.INSTANCE.toNote(
                        noteRepository.save(
                                NoteMapper.INSTANCE.toEntity(newNote)
                        )
                )
        );
    }

    @LogAround(level = Level.INFO)
    @Override
    public ResponseEntity<Void> deleteById(Long id) {
        noteRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @LogAround(level = Level.INFO)
    @Override
    public ResponseEntity<List<Note>> getAll(Optional<Integer> limit) {
        return ResponseEntity.ok(
                limit.map(l -> noteRepository.findAll(PageRequest.ofSize(l)).stream().toList())
                        .orElseGet(noteRepository::findAll)
                        .stream()
                        .map(NoteMapper.INSTANCE::toNote)
                        .toList()
        );
    }

    @LogAround(level = Level.INFO)
    @Override
    public ResponseEntity<Note> getById(Long id) {
        return ResponseEntity.ok(
                noteRepository.findById(id)
                        .map(NoteMapper.INSTANCE::toNote)
                        .orElseThrow()
        );
    }

    @LogAround(level = Level.INFO)
    @Override
    public ResponseEntity<Note> updateById(Long id, NewNote newNote) {
        return ResponseEntity.ok(
                NoteMapper.INSTANCE.toNote(
                        noteRepository.save(
                                NoteMapper.INSTANCE.toEntity(id, newNote)
                        )
                )
        );
    }
}
