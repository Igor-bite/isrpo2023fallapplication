package com.klyuzhevigor.isrpo.dal;

import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends ListCrudRepository<NoteEntity, Long>, ListPagingAndSortingRepository<NoteEntity, Long> {
}
