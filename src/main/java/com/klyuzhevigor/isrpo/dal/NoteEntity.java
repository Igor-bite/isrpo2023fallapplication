package com.klyuzhevigor.isrpo.dal;

import jakarta.validation.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("notes")
public record NoteEntity(
        @Id
        long id,
        @NotBlank
        String text
) {
}
