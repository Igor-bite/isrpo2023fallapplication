package com.klyuzhevigor.isrpo.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("otel")
public record OtelProperties(
        String collectorEndpoint,
        String appName
) {
}
