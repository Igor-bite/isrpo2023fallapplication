package com.klyuzhevigor.isrpo.configuration;

import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.exporter.otlp.trace.OtlpGrpcSpanExporter;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.SdkTracerProviderBuilder;
import io.opentelemetry.sdk.trace.SpanProcessor;
import io.opentelemetry.semconv.resource.attributes.ResourceAttributes;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(OtelProperties.class)
public class OtelConfiguration {

    @Bean
    SdkTracerProvider sdkTracerProvider(OtelProperties otelProperties, ObjectProvider<SpanProcessor> spanProcessors) {
        SdkTracerProviderBuilder sdkTracerProviderBuilder = SdkTracerProvider.builder()
                .setResource(Resource.create(
                        Attributes.of(
                                ResourceAttributes.SERVICE_NAME, otelProperties.appName()
                        )
                ));

        spanProcessors.orderedStream().forEach(sdkTracerProviderBuilder::addSpanProcessor);
        return sdkTracerProviderBuilder.build();
    }

    @Bean
    OtlpGrpcSpanExporter otlpGrpcSpanExporter(OtelProperties otelProperties) {
        return OtlpGrpcSpanExporter.builder()
                .setEndpoint(otelProperties.collectorEndpoint())
                .build();
    }
}
