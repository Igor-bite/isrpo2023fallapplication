CREATE TABLE IF NOT EXISTS notes
(
    id   BIGSERIAL PRIMARY KEY NOT NULL,
    text TEXT                  NOT NULL
);
